"""
Exercices.

Implémentez les méthodes ci-dessous.
Pour executer vos tests il vous faudra utiliser pytest (ou pas)
"""
import constants


def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float:
    """
    Exercice 1 :
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    if discount_rate < constants.MIN_DISCOUNT or discount_rate > constants.MAX_DISCOUNT:
        raise Exception("Invalid Discount Value")
    added_tax = htc_cost * constants.TAX_RATE
    after_tax = htc_cost + added_tax
    discount_applied = after_tax * discount_rate
    ttc_cost = after_tax - discount_applied

    return round(ttc_cost, 2)


def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """
    dividers = set()
    for number in range(2, value):
        if not value % number:
            dividers.add(number)
    if len(dividers) == 0:
        return "PREMIER"
    prime_dividers = set()
    for divider in dividers:
        if divisors(divider) == "PREMIER":
            prime_dividers.add(divider)
    return prime_dividers
